package cn.com.smart.form.dao;

import cn.com.smart.dao.impl.BaseDaoImpl;
import cn.com.smart.form.bean.entity.TFormScript;
import org.springframework.stereotype.Repository;

/**
 * @author 乌草坡 2019-09-10
 * @since 1.0
 */
@Repository
public class FormScriptDao extends BaseDaoImpl<TFormScript> {



}
